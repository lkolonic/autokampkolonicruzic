<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Parcela extends Model
{
    protected $guarded=[];

    public function deleteImage(){
        Storage::delete($this->image);
    }

    public function tip(){
        return $this->belongsTo(Tip::class, 'idTipParcela');
    }

    public function opis(){
        return $this->belongsTo(Opis::class, 'idOpisParcela');
    }
}
