<?php

namespace App\Http\Controllers;

use App\GostParcela;
use Illuminate\Http\Request;

class GostParcelaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexpregled()
    {
        return view('cmskontrola.pregled_gost_parcela');
    }

    public function indexpregledrezerviranihjedinica()
    {
        return view('cmskontrola.pregled_rezervirane_jedinice');
    }

    public function pregled(Request $request)
    {
        $datum = $request->datum;

        $gostiparcela = GostParcela::with('gost', 'parcela')->leftJoin('parcelas', 'parcelas.id', '=', 'gost_parcelas.idParcela')
        ->where('datumDolazak', '<=', $datum)
        ->where('datumOdlazak', '>=', $datum)->get();
        return view('cmskontrola.ispis_gost_parcela', compact('gostiparcela'));
    }

    public function pregledrezjed(Request $request)
    {
        $datum = $request->datum;

        $gostiparcela = GostParcela::with('gost', 'parcela')->leftJoin('parcelas', 'parcelas.id', '=', 'gost_parcelas.idParcela')
        ->where('datumDolazak', '=', $datum)->get();
        return view('cmskontrola.ispis_gost_parcela', compact('gostiparcela'));
    }

    public function pregledano($id)
    {
        
    }
    
}
